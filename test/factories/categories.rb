# frozen_string_literal: true

FactoryBot.define do
  factory :category do
    title { "MyString" }
    description { "MyText" }
  end
end
