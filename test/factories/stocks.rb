# frozen_string_literal: true

FactoryBot.define do
  factory :stock do
    price { 100 }
    quantity { 1 }
    reference
  end
end
