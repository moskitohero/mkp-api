# frozen_string_literal: true

FactoryBot.define do
  factory :reference do
    title { "MyString" }
    description { "MyText" }
    ean { "MyString" }
    category
  end
end
