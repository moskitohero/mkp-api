# frozen_string_literal: true

class Category < ApplicationRecord
  has_many :references, dependent: :nullify

  before_validation :set_uid, on: :create
  before_validation :set_slug, on: :create

  private

  def set_uid
    self.uid = SecureRandom.uuid
  end

  def set_slug
    self.slug = "#{title.parameterize}-#{SecureRandom.hex(2)}"
  end
end
