# frozen_string_literal: true

class Stock < ApplicationRecord
  belongs_to :reference
  has_one :category, through: :reference

  before_validation :set_uid, on: :create

  private

  def set_uid
    self.uid = SecureRandom.uuid
  end
end
