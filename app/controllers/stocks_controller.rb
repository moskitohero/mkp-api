# frozen_string_literal: true

class StocksController < ApplicationController
  before_action :set_stock, only: %i[show update destroy]

  # GET /stocks
  def index
    @stocks = Stock.all

    render json: @stocks
  end

  # GET /stocks/1
  def show
    render json: @stock
  end

  # POST /stocks
  def create
    @stock = Stock.new(stock_params)


    if @stock.save
    build_body
      conn = Faraday.new(url: "https://dummyjson.com", headers: { "Content-Type" => "application/json" })
      response = conn.post("/products/add", @body)
      @stock.update(dummyjson_id: JSON.parse(response.body)["id"])

      render json: @stock, status: :created, location: @stock
    else
      render json: @stock.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /stocks/1
  def update
    if @stock.update(stock_params)
    build_body
      conn = Faraday.new(url: "https://dummyjson.com", headers: { "Content-Type" => "application/json" })
      conn.put("/products/update/#{@stock.dummyjson_id}", @body)
      render json: @stock
    else
      render json: @stock.errors, status: :unprocessable_entity
    end
  end

  # DELETE /stocks/1
  def destroy
    conn = Faraday.new(url: "https://dummyjson.com", headers: { "Content-Type" => "application/json" })
    conn.delete("/products/delete/#{@stock.dummyjson_id}")
    @stock.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_stock
    @stock = Stock.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def stock_params
    params.require(:stock).permit!
  end

  def build_body
    @body = {
      "product": {
        "title": @stock.reference.title,
        "description": @stock.reference.description,
        "price": @stock.price,
        "category": @stock.reference.category.title
      }
    }.to_json
  end
end
