# frozen_string_literal: true

class CreateReferences < ActiveRecord::Migration[6.0]
  def change
    create_table :references do |t|
      t.string :ean
      t.boolean :archived, default: false
      t.string :uid, unique: true
      t.string :title
      t.text :description
      t.references :category, null: false, foreign_key: true
      t.string :slug, unique: true

      t.timestamps
    end
  end
end
