class AddStockDummyJsonId < ActiveRecord::Migration[6.0]
  def change
    add_column :stocks, :dummyjson_id, :integer
  end
end
