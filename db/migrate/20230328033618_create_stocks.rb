# frozen_string_literal: true

class CreateStocks < ActiveRecord::Migration[6.0]
  def change
    create_table :stocks do |t|
      t.references :reference, null: false, foreign_key: true
      t.float :price
      t.boolean :archived
      t.integer :quantity
      t.string :uid, unique: true

      t.timestamps
    end
  end
end
