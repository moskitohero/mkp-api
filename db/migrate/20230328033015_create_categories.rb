# frozen_string_literal: true

class CreateCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :categories do |t|
      t.string :uid, unique: true
      t.string :slug, unique: true
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
