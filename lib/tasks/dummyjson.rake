namespace :dummyjson do
  desc "Import dummyjson data"
  task import: :environment do
    conn = Faraday.new(url: "https://dummyjson.com", headers: {"Content-Type" => "application/json"})
    response = conn.get("/products")
    json = JSON.parse(response.body)
    json["products"].each do |product|
      category = Category.find_or_create_by(title: product["category"])
      reference = Reference.find_or_create_by(
        title: product["title"],
        description: product["description"],
        category: category
      )
      Stock.create(
        reference: reference,
        price: product["price"],
        dummyjson_id: product["id"],
        quantity: rand(1..100)
      )
    end
  end
end
