# README

This is a simple rails app

## Requirements
- Ruby 3.0.5
- Docker

## Setup
```shell
bundle install
rake db:create
rake db:migrate
```

## Load dummy data
```shell
rails dummyjson:import
```

## Run
### Start the server
```shell
bundle exec rails s
```

### Sidekiq
```shell
docker-compose up -d
bundle exec sidekiq
```
