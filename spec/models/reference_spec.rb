# frozen_string_literal: true

require "rails_helper"

RSpec.describe Reference, type: :model do
  describe "uid" do
    let(:reference) { build(:reference) }
    it "is set before validation" do
      expect(reference.uid).to be_nil
      reference.valid?
      expect(reference.uid).to be_present
    end

    it "is unique" do
      reference1 = create(:reference)
      reference2 = create(:reference)
      expect(reference1.uid).not_to eq(reference2.uid)
    end
  end

  describe "slug" do
    let(:reference) { build(:reference) }
    it "is set before validation" do
      expect(reference.slug).to be_nil
      reference.valid?
      expect(reference.slug).to be_present
    end

    it "is unique" do
      reference1 = create(:reference)
      reference2 = create(:reference)
      expect(reference1.slug).not_to eq(reference2.slug)
    end
  end
end
