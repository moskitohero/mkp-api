# frozen_string_literal: true

require "rails_helper"

RSpec.describe Stock, type: :model do
  describe "uid" do
    let(:stock) { build(:stock) }
    it "is set before validation" do
      expect(stock.uid).to be_nil
      stock.valid?
      expect(stock.uid).to be_present
    end

    it "is unique" do
      stock1 = create(:stock)
      stock2 = create(:stock)
      expect(stock1.uid).not_to eq(stock2.uid)
    end
  end
end
