# frozen_string_literal: true

require "rails_helper"

RSpec.describe Category, type: :model do
  describe "uid" do
    let(:category) { build(:category) }
    it "is set before validation" do
      expect(category.uid).to be_nil
      category.valid?
      expect(category.uid).to be_present
    end

    it "is unique" do
      category1 = create(:category)
      category2 = create(:category)
      expect(category1.uid).not_to eq(category2.uid)
    end
  end

  describe "slug" do
    let(:category) { build(:category) }
    it "is set before validation" do
      expect(category.slug).to be_nil
      category.valid?
      expect(category.slug).to be_present
    end

    it "is unique" do
      category1 = create(:category)
      category2 = create(:category)
      expect(category1.slug).not_to eq(category2.slug)
    end
  end
end
